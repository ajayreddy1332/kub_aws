####### Aws setup for the KUbernetes Cluster ##########

# Configure the Terraform backend

resource "aws_s3_bucket" "terraform-state-storage-s3" {
    bucket = "spash-framework"
    acl    = "private"
 
    versioning {
      enabled = true
    }
 
    lifecycle {
      prevent_destroy = true
    }
 
    tags {
      Name = "S3 Remote Terraform State Store"
    }      
}

resource "aws_s3_bucket_policy" "backend_s3_policy" {
  bucket = "${aws_s3_bucket.terraform-state-storage-s3.bucket}"
  policy =<<POLICY
{
  "Version": "2012-10-17",
  "Id": "MYBUCKETPOLICY",
  "Statement": [
    {
      "Sid": "IPAllow",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:*",
      "Resource": "arn:aws:s3:::spash-framework/*"
    } 
  ]
}
POLICY
}


data "terraform_remote_state" "Store_tf_state"{
  backend = "s3" 
    config {
    bucket = "${aws_s3_bucket.terraform-state-storage-s3.bucket}"
    region = "us-east-1"
    key    = "terraform.tfstate"
    profile = "default"
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  profile = "default"
}

