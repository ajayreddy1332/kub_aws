variable "ec2_name" {
  description = "The name of the EC2 Instance."
  default ="stack"
}

variable "key_pair_name" {
  description = "The name of a Key Pair that you've created in AWS and have saved on your computer. You will be able to use this Key Pair to SSH to the EC2 instance."
  default = "spash3" 
}