



output "resourceVPCID" {
  value = "${aws_vpc.resourceVPC.id}"
}


output "natsecid" {
  value = ["${aws_security_group.nat_sec.id}"]
}

output "igw-id" {
   value = ["${aws_internet_gateway.default.id}"]
}

output "subnet_public_id" {

value = ["${aws_subnet.us-east-1a-public.id}"]

}

output "subnet_public_cidrs"
{
    value =["${aws_subnet.us-east-1a-public.cidr_block}"]
}

output "subnet_private_id" {

value = ["${aws_subnet.us-east-1a-private.id}"]

}

output "subnet_private_cidrs"
{
    value =["${aws_subnet.us-east-1a-private.cidr_block}"]
}


