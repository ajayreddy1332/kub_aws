
terraform  {
  backend "s3"  {
    bucket = "spash-framework"
    region = "us-east-1"
    key    = "resourceVPC.tfstate"
    profile = "default"
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  profile = "default"
}

resource "aws_vpc" "resourceVPC" {
    cidr_block = "${var.vpc_cidr}"
    enable_dns_hostnames = true
    tags {
        Name = "Resource-aws-vpc"
    }
}

resource "aws_internet_gateway" "default" {
    vpc_id = "${aws_vpc.resourceVPC.id}"
}


######  NAT Instance #####
resource "aws_security_group" "nat_sec" {
    name = "nat_sec"
    description = "Allow traffic to pass from the private subnet to the internet"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["${var.private_subnet_cidr}"]
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["${var.private_subnet_cidr}"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }
    egress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.resourceVPC.id}"

    tags {
        Name = "NATSEC"
    }
}

resource "aws_instance" "natserver" {
    ami = "ami-01623d7b" # this is a special ami preconfigured to do NAT
    availability_zone = "us-east-1a"
    instance_type = "t2.micro"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.nat_sec.id}"]
    subnet_id = "${aws_subnet.us-east-1a-public.id}"
    associate_public_ip_address = true
    source_dest_check = false

    tags {
        Name = " NAT"
    }
}

resource "aws_eip" "nat" {
    instance = "${aws_instance.natserver.id}"
    vpc = true
}

/*
  Public Subnet
*/
resource "aws_subnet" "us-east-1a-public" {
    vpc_id = "${aws_vpc.resourceVPC.id}"

    cidr_block = "${var.public_subnet_cidr}"
    availability_zone = "us-east-1a"

    tags {
        Name = "Public Subnet"
    }
}

resource "aws_route_table" "us-east-1a-public" {
    vpc_id = "${aws_vpc.resourceVPC.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.default.id}"
    }

    tags {
        Name = "Public Subnet"
    }
}

resource "aws_route_table_association" "us-east-1a-public" {
    subnet_id = "${aws_subnet.us-east-1a-public.id}"
    route_table_id = "${aws_route_table.us-east-1a-public.id}"
}

/*
  Private Subnet
*/
resource "aws_subnet" "us-east-1a-private" {
    vpc_id = "${aws_vpc.resourceVPC.id}"

    cidr_block = "${var.private_subnet_cidr}"
    availability_zone = "us-east-1a"

    tags {
        Name = "Private Subnet"
    }
}

resource "aws_route_table" "us-east-1a-private" {
    vpc_id = "${aws_vpc.resourceVPC.id}"

    route {
        cidr_block = "0.0.0.0/0"
        instance_id = "${aws_instance.natserver.id}"
    }

    tags {
        Name = "Private Subnet"
    }
}

resource "aws_route_table_association" "us-east-1a-private" {
    subnet_id = "${aws_subnet.us-east-1a-private.id}"
    route_table_id = "${aws_route_table.us-east-1a-private.id}"
}



##### network ACL for the public subnet ##########

resource "aws_network_acl" "resource-public"{
vpc_id ="${aws_vpc.resourceVPC.id}"
subnet_ids =["${aws_subnet.us-east-1a-public.id}"]
tags{

    Name = "resource_public"
}
}

#####nacl ingress rules public subnet ################

resource "aws_network_acl_rule" "resource-public-ingress1" {
  network_acl_id = "${aws_network_acl.resource-public.id}"
  rule_number    = 100
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 80
  to_port        = 80
}

resource "aws_network_acl_rule" "resource-public-ingress2" {
  network_acl_id = "${aws_network_acl.resource-public.id}"
  rule_number    = 101
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 443
  to_port        = 443
}

resource "aws_network_acl_rule" "resource-public-ingress3" {
  network_acl_id = "${aws_network_acl.resource-public.id}"
  rule_number    = 102
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}

resource "aws_network_acl_rule" "resource-public-ingress4" {
  network_acl_id = "${aws_network_acl.resource-public.id}"
  rule_number    = 103
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 0
  to_port        = 0
}


#####nacl egress rules public subnet ################

resource "aws_network_acl_rule" "resource-public-egress1" {
  network_acl_id = "${aws_network_acl.resource-public.id}"
  rule_number    = 100
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 80
  to_port        = 80
}
resource "aws_network_acl_rule" "resource-public-egress2" {
  network_acl_id = "${aws_network_acl.resource-public.id}"
  rule_number    = 101
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 443
  to_port        = 443
}resource "aws_network_acl_rule" "resource-public-egress3" {
  network_acl_id = "${aws_network_acl.resource-public.id}"
  rule_number    = 102
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}resource "aws_network_acl_rule" "resource-public-egress4" {
  network_acl_id = "${aws_network_acl.resource-public.id}"
  rule_number    = 103
  egress         = true
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 0
  to_port        = 0
}



##### network ACL for the private subnet ##########

resource "aws_network_acl" "resource-private"{
vpc_id ="${aws_vpc.resourceVPC.id}"
subnet_ids =["${aws_subnet.us-east-1a-private.id}"]
tags{

    Name = "resource_private"
}
}

#####nacl ingress rules private subnet ################



resource "aws_network_acl_rule" "resource-private-ingress1" {
  network_acl_id = "${aws_network_acl.resource-private.id}"
  rule_number    = 100
  egress         = false
  protocol       = "all"
  rule_action    = "allow"
  cidr_block     = "10.0.0.0/8"
  from_port      = 0
  to_port        = 0
}


#####nacl egress rules private subnet ################

resource "aws_network_acl_rule" "resource-private-egress1" {
  network_acl_id = "${aws_network_acl.resource-private.id}"
  rule_number    = 100
  egress         = true
  protocol       = "all"
  rule_action    = "allow"
  cidr_block     = "10.0.0.0/8"
  from_port      = 0
  to_port        = 0
}











