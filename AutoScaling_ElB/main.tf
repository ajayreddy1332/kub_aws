provider "aws" {
  region = "${var.aws_region}"
}

terraform  {
  backend "s3"  {
    bucket = "spash-framework"
    region = "us-east-1"
    key    = "Autoscaling_ELB.tfstate"
    profile = "default"
  }
}


data "terraform_remote_state" "resourcevpcnetwork"{
backend ="s3"
config {
    bucket = "spash-framework"
    region = "us-east-1"
    key    = "resourceVPC.tfstate"
    profile = "default"
  }

}


resource "aws_elb" "web-elb" {
  name = "terraform-example-elb"
  subnets= ["${data.terraform_remote_state.resourcevpcnetwork.subnet_private_id}"]
  security_groups =["${aws_security_group.KUB-elb.id}"]


  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }
}

resource "aws_autoscaling_group" "KUb-asg" {
  count                = 3  
  availability_zones   = ["us-east-1a"]
  name                 = "${element(split(",",var.Names),count.index)}"
  max_size             = "${var.asg_max}"
  min_size             = "${var.asg_min}"
  desired_capacity     = "${var.asg_desired}"
  force_delete         = true
  launch_configuration = "${aws_launch_configuration.KUB-lc.name}"
  load_balancers       = ["${aws_elb.web-elb.name}"]
  vpc_zone_identifier = ["${data.terraform_remote_state.resourcevpcnetwork.subnet_private_id}"]
  tag {
    key                 = "Name"
    value               = "${element(split(",",var.Names),count.index)}"
    propagate_at_launch = "true"
  }
}

resource "aws_launch_configuration" "KUB-lc" {
  name          = "KUB-lc"
  image_id      = "${var.aws_amis}"
  instance_type = "${var.instance_type}"

  # Security group
  security_groups = ["${aws_security_group.KUB-sec.id}"]
  #user_data       = "${file("userdata.sh")}"
  key_name        = "${var.key_name}"
}

# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "KUB-sec" {
  name        = "KUB_def_sg"
  description = "Used in the terraform"
  vpc_id      = "${data.terraform_remote_state.resourcevpcnetwork.resourceVPCID}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "KUB-elb" {
  name        = "KUB_elb_sg"
  description = "Used in the terraform"
  vpc_id      = "${data.terraform_remote_state.resourcevpcnetwork.resourceVPCID}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}